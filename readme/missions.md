### [<- Back to index](../readme.md "readme")

Missions
========
Missions is the page which contains the future and past space missions.

Missions contains:

- A navigation bar
- A list of future missions (that can be scrolled using the dedicated buttons) and with each a description that can be viewed by clicking on it.
- A list of old missions (that can be scrolled using the dedicated buttons) and with each a description that can be viewed by clicking on it.
- A list of current missions (that can be scrolled using the dedicated buttons) and with each a description that can be viewed by clicking on it.
- A footer with an anchor to our abouts.

Responsiveness
--------------
The page is fully responsive thanks to the use of the **media queries**. The media queries present in this case are **two**:

- A media query for the **desktop version** (for all the 
    devices that have at least 1100px of width)

- A media query for the **mobile version** (for all the 
    devices that have a width lower than 1100px)


Navigation Bar
--------------
The navigation bar has been added to grant a form of navigation across all the pages to the user, when the latter enters in the page. The navigation bar contains the logo of the organization alongside some navigation links.
The navigation bar has been coded using the semantic tags (
in this case with the ```<nav>``` tag ).


#### Navigation buttons
The navigation buttons have been built using anchors and
have been correctly spaced and defined using the dedicated 
CSS file "index-nav.css" located in ```style/reset-and-misc/navbar-footer.css```.

The navigation buttons have been also shaped to be correctly responsive.

#### Responsiveness
The navigation bar responsiveness changes based on the media queries:

- If the user is viewing the **desktop version** the navigation bar
  assumes a classic horizontal style

- If the user is viewing the **mobile version**, the 
  navigation bar is converted to a dropdown vertical type
  that is shown only if the user press a dedicated button.


Future, Old and Current Missions Section
---------------------------------------
These are three different sections but which use exactly the same code
They contains the **missions** displayed as a gallery of cards that can 
be scrolled using the two buttons. Each mission can be clicked, 
and for each a different description will appear.

Descriptions consist in a **code**, a **destination**, the **equipment used**
a description text and a link to astronauts page.

All the images and the text have been properly styled thanks to
the CSS located in ```style\mission\desktop-version.css``` 
for desktop version, and in ```style\mission\mobile-version.css``` 
for the mobile version.
(some fixes for both versions are in instead ```style\mission\header-nav.css```)

#### Scrolling
The scroll of the missions has been made possible by JavaScript.
The function adds and removes some **special classes** (
called **service css rules** in the CSS file) 
to remove / add the elements, creating the scrolling to the eyes
of the user

#### Popup description
The description use js for add the same classes used for scrolling but at
the specific id of the **mission**.
I used two function, one for open and one for close.
If you click on image open function are triggered and **hide** class are removed.
Other way around if you click on close button **hide** class are added.


#### Responsiveness
In particular:

- If the user is viewing the **Desktop version**, every missions type
  displays 2 elements at the time.

- If the user is viewing the **Mobile version**,
  the missions are one below the other with the description visible
  without having to click on the image.
  


Footer
------
The footer displays a link that redirects to the about us 
page of the site. The footer has been styled using its 
dedicated CSS file located in: ```style/reset-and-misc/navbar-footer.css```


#### Responsiveness
The footer has been also styled to be fully responsive.

The main changes that affects the footer implementing the responsiveness are the dimensions of the proper
 ```<footer>``` element and the dimensions of the text and 
the logo inside it (in the **mobile version** they are smaller). 

### [<- Back to index](../readme.md "readme")