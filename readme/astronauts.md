### [<- Back to index](../readme.md "readme")

Astronauts
========
Astronauts is the page where all the active and retired astronauts are listed.

Astronauts cotains: 

- a navigation bar
- one section for the active astronauts
- one section for the retired astronauts
- a footer with an anchor to our abouts

Responsiveness
--------------
The page is fully responsive thanks to the use of the **media queries**. The media queries present in this case are **two**:

- A media query for the **desktop version** (for all the 
    devices that have at least 1100px of width)

- A media query for the **mobile version** (for all the 
    devices that have a width lower than 1100px)

Navigation Bar
--------------
The navigation bar has been added to grant a form of navigation across all the pages to the user, when the latter enters in the page. The navigation bar contains the logo of the organization alongside some navigation links.
The navigation bar has been coded using the semantic tags (
in this case with the ```<nav>``` tag ).


#### Navigation buttons
The navigation buttons have been built using anchors and
have been correctly spaced and defined using the dedicated 
CSS file "index-nav.css" located in ```style/reset-and-misc/navbar-footer.css```.

The navigation buttons have been also shaped to be correctly responsive.

#### Responsiveness
The navigation bar responsiveness changes based on the media queries:

- If the user is viewing the **desktop version** the navigation bar
  assumes a classic horizontal style

- If the user is viewing the **mobile version**, the 
  navigation bar is converted to a dropdown vertical type
  that is shown only if the user press a dedicated button.

Active and Retired astronauts sections
--------------
These are 2 different sections that use the same code.

The first one is the **Active Astronauts** section.
It contains all the active astronauts listed as image cards, but when you hover on them they flip and show all the information you need about the astronaust, like *name*, *age*, *nationality*, a *short bio* and the *space in time*.

All the astronauts images are stored in the ```images\astronauts``` folder, all named in kebab code. 
All the images and the text have been properly styled thanks to
the CSS located in ```style\astronauts\mystyle.css```.

#### Cards flip animation
The card flip animation is made only by html and css.
The css rules flip and style the cards.
All was blocked in line and centered on the screen.
All the images are modified to be squares and that is fondamental for the page responsivness.

#### Responsiveness

In particular: 
- the viewer can see 1,2,3 or more cards all inline, all based on his screen resolution
- if the screen resolution is too small, you will see only 1 card on every line and the cards gets smaller if it needs

Footer
------
The footer displays a link that redirects to the about us 
page of the site. The footer has been styled using its 
dedicated CSS file located in: ```style/reset-and-misc/navbar-footer.css```


#### Responsiveness
The footer has been also styled to be fully responsive.

The main changes that affects the footer implementing the responsiveness are the dimensions of the proper
 ```<footer>``` element and the dimensions of the text and 
the logo inside it (in the **mobile version** they are smaller).

### [<- Back to index](../readme.md "readme")
