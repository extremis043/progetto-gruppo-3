### [<- Back to index](../readme.md "readme")

Join Us
========
In this page we had created a registration form where site visitors can apply for future missions.

Join Us contains:

- A navigation bar
- A registration form where users can sign up.
- A footer with an anchor to our abouts.

Responsiveness
--------------
The page is fully responsive thanks to the use of the **media queries**. The media queries present in this case are **two**:

- A media query for the **desktop version** (for all the 
    devices that have at least 1100px of width)

- A media query for the **mobile version** (for all the 
    devices that have a width lower than 1100px)


Navigation Bar
--------------
The navigation bar has been added to grant a form of navigation across all the pages to the user, when the latter enters in the page. The navigation bar contains the logo of the organization alongside some navigation links.
The navigation bar has been coded using the semantic tags (
in this case with the ```<nav>``` tag ).


#### Navigation buttons
The navigation buttons have been built using anchors and
have been correctly spaced and defined using the dedicated 
CSS file "index-nav.css" located in ```style/reset-and-misc/navbar-footer.css```.

The navigation buttons have been also shaped to be correctly responsive.

#### Responsiveness
The navigation bar responsiveness changes based on the media queries:

- If the user is viewing the **desktop version** the navigation bar
  assumes a classic horizontal style

- If the user is viewing the **mobile version**, the 
  navigation bar is converted to a dropdown vertical type
  that is shown only if the user press a dedicated button.

Forms
--------------
These forms ar all client-side validated. to make this possible we used specialized input type like 'email' or 'password'.
Data from forms are sent to a PHP application which store them in a JSON file. Then the application give a feedback to the user about the success of the operation showing him a confirm page calling him with the name which entered in the First Name field.
If the operation isn't successful the application response with a user-readable error which explain what went wrong.

#### Responsiveness
The navigation bar responsiveness changes based on the media queries:

- If the user is viewing the **desktop version** the form section width is the 50% of the total width. For desktop PCs wtith low resolutions this section is 20% bigger.
- If the user is viewing the **mobile version**, the forms section will occupy the 95% of the viewport and it lose the top and bottom margins.


Footer
------
The footer displays a link that redirects to the about us 
page of the site. The footer has been styled using its 
dedicated CSS file located in: ```style/reset-and-misc/navbar-footer.css```


#### Responsiveness
The footer has been also styled to be fully responsive.

The main changes that affects the footer implementing the responsiveness are the dimensions of the proper
 ```<footer>``` element and the dimensions of the text and 
the logo inside it (in the **mobile version** they are smaller).

registration.php
--------------
This is a PHP page which elaborate data from join us forms and can give dinamically responses about the succes of the registration.

### [<- Back to index](../readme.md "readme")