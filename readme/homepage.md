### [<- Back to index](../readme.md "readme")

Homepage                
========
The homepage is the main page that the user sees when opening the site (the index.html)

The homepage contains:

- A navigation bar
- A list of missions (that can be scrolled using the dedicated buttons)
- A gallery that contains the photos of the missions.
- A footer with an anchor to our abouts.

Responsiveness
--------------
The page is fully responsive thanks to the use of the **media queries**. The media queries present in this case are **two**:

- A media query for the **desktop version** (for all the 
    devices that have at least 800px of width)

- A media query for the **mobile version** (for all the 
    devices that have a width lower than 800px)


Navigation Bar
--------------
The navigation bar has been added to grant a form of navigation across all the pages to the user, when the latter enters in the page. The navigation bar contains the logo of the organization alongside some navigation links.
The navigation bar has been coded using the semantic tags (
in this case with the ```<nav>``` tag ).


#### Navigation buttons
The navigation buttons have been built using anchors and
have been correctly spaced and defined using the dedicated 
CSS file "index-nav.css" located in ```style\homepage\index.nav.css```.

The navigation buttons have been also shaped to be correctly responsive.

#### Responsiveness
The navigation bar responsiveness changes based on the media queries:

- If the user is viewing the **desktop version** the navigation bar
  assumes a classic orizontal style

- If the user is viewing the **mobile version**, the 
  navigation bar is converted to a dropdown vertical type
  that is shown only if the user press a dedicated button.


News Section
-------------
The news section contains a **series of news** (composed by an
image and a text) displayed as a gallery of cards that can 
be scrolled using the two buttons.

All the images and the text have been properly styled to be 
consistent and fit the main widths of the devices, thanks to
the CSS located in ```style\homepage\homepage-main.css``` 
(the news section is identified with the id ```#news-section```)

#### Scrolling
The scroll of the news has been made possible by JavaScript.
The function adds and removes some **special classes** (
called **service css rules** in the CSS file) 
to remove / add the elements, creating the scrolling to the eyes
of the user

#### Responsiveness
The responsiveness of the navbar has been implemented using 
the media queries declared before.
In particular:

- If the user is viewing the **Desktop version**, the news 
  section displays 3 elements at the time.

- If the user is viewing the **Mobile version**, the news
  section displays **2** elements at the time. This choice 
  was made to avoid having too small news in the smaller 
  displays. 
  
This mobile version's configuration was accomplished using 
a combination of both JS (calling it at the end of file) and CSS.
In particular, the JS uses a special function called 
**matchmedia**(compatible with most browsers) that allows a 
control of a media query passed as a parameter:

```javascript
const mediaQuery=window.matchMedia(" Media query here ");
```

In addition, the page uses a particular property called 
matches. It assumes a value of true if the device matches 
the media query, otherwise it returns false. Below is shon an example:

```javascript
const mediaQuery=window.matchMedia(" ( min-width:800 ) ");
if(mediaQuery.matches){
    // The device matches the media query
}else{
    // The device does NOT match the media query
}
```
More information is avaiable on: [The W3schools reference](https://www.w3schools.com/howto/howto_js_media_queries.asp)


Gallery Section
---------------
The gallery section contains a set of images displayed as single cards 
that can be scrolled using the dedicated buttons.
The images have been styled to be consistent and be 
compatible with the site responsiveness with their CSS rules
located in: ```style\homepage\homepage-main.css``` (the gallery section 
is identified with the id ```#gallery-section```).

#### Scrolling
The scrolling is enabled with a JS function similar to the news scrolling. It uses the same concept of the **special classes** seen in the news section.

#### Responsiveness
The gallery has been made responsive using some CSS to 
change the size of the image based on the type of media 
query the user sees in the moment (the size is **smaller** in the **Mobile version**).


Footer
------
The footer displays a link that redirects to the about us 
page of the site. The footer has been styled using its 
dedicated CSS file located in: ```homepage\homepage-footer.css```


#### Responsiveness
The footer has been also styled to be fully responsive using
the media queries declared in the beginning.

The main changes that affects the footer implementing the responsiveness are the dimensions of the proper
 ```<footer>``` element and the dimensions of the text and 
the logo inside it (in the **mobile version** they are smaller). 

### [<- Back to index](../readme.md "readme")