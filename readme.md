# Progetto Space
## _Authors information_
- Group Name: fsociety
- Group Coordinator: Erik Barale
- \* Group Components: Erik Barale, Alessandro Incorvaia, Carlos Palomino, Ilya Pizzo 
- Course: ITS Backend

<br>\* we haven't assigned individual roles, we worked all together
## _Requirements_ 
### Homepage
The homepage should include a quick introduction to the website to help users identify what's it about and to
navigate to more in-depth content
It should include a content area for the latest news posts (at least 3). The content area could be a sidebar or
another container
It should also include some information from each of the other areas (missions, astronauts, join us,, gallery,
about) as well as CTAs (call to action) to navigate to those areas
### Missions
This area should include a list of missions (at least 20). They should be grouped in three sections: past, current
and future missions. Each mission should include at least a name, code, destination, image, equipment, crew.
The crew should include links to the astronauts page and to the specific astronauts for each mission
The missions page should be responsive, list in mobile view and cards on larger viewports
### Astronauts
This area should include a list of astronauts (at least 20). They should be grouped in two sections: active and
retired. Each astronaut should include at least a name, code, bio, photo, age, nationality, years of experience
and links to missions in which the astronaut has participated
The astronauts page should be responsive, list in mobile view and cards on larger viewports
### Join us
This area should contain an application form to gather information from future astronaut candidates:
- First, middle (optional) and last names
- Desired mission (Choice of missions should match future missions in the missions area)
- Age, gender, hair and eye color (color picker or choose from a list)
- Contact information: email, phone numbers, address, etc
- Weight (max 100kg - sorry in space weight is limited)
- A shot biography (max 255 characters)
- Any other information that you want
The form must be fully client-side validated
### Gallery
This section includes images from previous missions, future missions, crew landings and any other space
related images
You are free to design it as you see fit, however it should be fully responsive
### About
This section includes some information about the agency, what it does, its history and any other relevant
information
You are free to design it as you see fit, however it should be fully responsive
Responsiveness
All the pages of the website should be fully responsive and should be designed mobile-first
Responsiveness does not just mean that the website is functional at different viewport sizes but that the
device and screen real estate are taken into consideration for the user experience (appropriate font sizes,
element positioning, navigation aids, etc.)
### Optional
- create another area where you can embed some videos about space from youtube or vimeo. Don't exaggerate
this, think about performance and the user experience
- create another area called 'destinations' with a list of previous and possible destinations for the missions.
Provide some images and details for each. Link missions to destinations
### Important
- Use meaningful names (for files, classes, id's, etc.)
- The code should be well documented
- The code should be well indented
- The data structures should be effective and the code should be efficient
- You need to be able to explain each line of your code
- Pay attention to feedback given for previous exercises and projects. Any issues should not be repeated
### Bonus (extra points)
Bonus 1: Animations and transitions <br>

Use transitions and animations to change the look and feel of your application in some instances, e.g. when
switching from one breakpoint to another, or when the user interacts with your application (e.g. hover, focus,
etc)
Transitions and animations should be appropriate to the situation and not exaggerated
Using CSS transitions - CSS: Cascading Style Sheets
Using CSS animations - CSS: Cascading Style Sheets
css-tricks - animation
This Ain't Disney: A practical guide to CSS transitions and animations

Bonus 2: Backend form handling <br>
- build a small back-end server to process and display received astronaut applications
- any back-end language may be used (php, python, nodeJS, etc)
- the readme.md file should include detailed information on how to run and access the back-end
- any web server (local or remote) may be used to host the application
- the back-end application should have a web page that displays the list of applications received. This page
should be linked to from the front-end website

## _Structure of website_
In this project we have divided the website in five main pages: The homepage, the mission page, the astronaut page, the Join Us page and the About Us. In the homepage the navigation bar is more complex and elaborate to guarantee a better first impact by the user, while in the other pages it is always with the same height with a responsive menu.
In all pages the css code is structured by elements to make it more simple to read.
Javascript is used to create the responsive menu for small devices and to create the transition on homepage images. 

 ### [Homepage](readme/homepage.md "Homepage")
    The homepage is the main page that the user sees when opening the site (the index.html). 
    Click on the title for more info about this page.
### [Missions](readme/missions.md "Missions")
    In this page there are the future and past space missions.
    Click on the title for more info about this page.
### [Astronauts](readme/astronauts.md "Astronauts")
    ------
### [Join Us](readme/joinus.md "Join Us")
    In this page we had created a registration form where site visitors can apply for future missions.
    Click on the title for more info about this page.
### About Us
    This page contains only the description of the company and what they do

## _Instructions to run the website_
To make this website working you have to install Apache and PHP:
To do this you should use XAMPP (or LAMP if you have a Unix- based OS) and copy the entire project folder in the HTDOCS folder of Apache.
Now from the XAMPP interface you have to start Apache web server, open your browser and connect to http://localhost:8080/ProgettoSpace/index.html and enjoy our website ! (8080 is the default port of Apache, if you have changed it you also have to change this URL).

## _Technical Details_
This website contains only test data.<br>
To make it working it should be stored on a web server like Apache or Nginx and connected to a database with real data.<br>
To create this page and keep versioning of files we used a git softare.

## _Validation and test_
Tested browser:
- Google chrome v80
- Firefox v74
- Internet Explorer 11
- Opera
- Vivaldi
- Edge v44
- Chromium based Browsers

Validated on [W3C Validator](https://validator.w3.org/ "W3C Validator")
