<?php 
$error = true;
$candidate = array();
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
    $error=false;
    $candidate = [ 
        "firstName" => $_POST["firstName"],
        "middleName" => $_POST["middleName"],
        "lastName" => $_POST["lastName"],
        "gender" => $_POST["gender"],
        "weight" => $_POST["weight"],
        "eyecolor" => $_POST["eyecolor"],
        "haircolor" => $_POST["haircolor"],
        "birthdate" => $_POST["birthdate"],
        "mission" => $_POST["mission"],
        "bio" => $_POST["bio"],
        "email" => $_POST["email"],
        "tel" => $_POST["tel"],
        "country" => $_POST["country"],
        "city" => $_POST["city"],
        "address" => $_POST["address"]
    ];
}

if($candidate["mission"]=="")
    $error = true;

$fp = fopen("candidates.json", "a+");
if(!$fp)
    $error = true;

fwrite($fp, json_encode($candidate));
fclose($fp);
?>
<!doctype html>
<html lang="it">

<head>
    <meta charset="utf-8">
    <title>Join Us</title>
    <!-- metadata -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- change the icon of the page -->
    <link rel="icon" href="images/logo/logo.png" type="image/x-icon">
    <!-- Montserrat google font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@1,300&display=swap" rel="stylesheet">
    <!-- navbar style -->
    <link rel="stylesheet" type="text/css" href="style/reset-and-misc/navbar-footer.css">
    <!-- content style -->
    <link rel="stylesheet" href="style/joinus/style-confirmation.css">
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- shiv -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
</head>

<body>
	<header class="header">
		<a href="index.html" class="header-logo"><img src="images\logo\logo-inline.png"></a>
		<a href="" class="header-icon-bar">
			<span></span>
			<span></span>
			<span></span>
		</a>
		<ul class="header-menu animate">
			<li class="header-menu-item"><a href="index.html">Home</a></li>
			<li class="header-menu-item"><a href="missions.html">Missions</a></li>
            <li class="header-menu-item"><a href="astronauts.html">Astronauts</a></li>
            <li class="header-menu-item"><a href="aboutus.html">About us</a></li>
        </ul>
        <div class="header-button">
			<span class="header-button-line"></span>
			<span class="header-button-line"></span>
			<span class="header-button-line"></span>
		</div>
		</ul>
	</header>

    <main class="content">

        <div class="content-text">
            <p>
                <?php 
                if(!$error)
                {
                    echo "<h1>Thanks for your registration ".$candidate["firstName"]."!</h1>
                    we will contact you for more informations";
                }
                else if($candidate["mission"]=="" && $error)
                {
                    echo "<h2>Complete all the required fields!</h2><br>
                    <a href=\"javascript:history.back()\">go back to correct and resend the form</a>";
                    // history.back() js function simulate the "back" button of browser
                }
                else
                {
                    echo "<h2>An error occurred processing the form</h2><br>
                    Checkthe filds entered or try again later<br>
                    <a href=\"javascript:history.back()\">go back to correct and resend the form</a>";
                    // history.back() js function simulate the "back" button of browser
                }
                ?>
            </p>
        </div>
    </main>
    <footer class="footer">
        <img src="images\logo\logo-inline.png" alt="fsociety">
        <h3>
            Click here to learn more <a href="aboutus.html">about us</a></h3>
    </footer>
    <!-- javascript for responsive navbar -->
    <script src="js/responsive-navbar.js"></script>
</body>
</html>