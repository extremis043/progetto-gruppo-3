/**
 * @file: missioni-descrizioni.js
 * @author: Fsociety
 * Purpose of file
 *
 * Utilizzato per i popup di descrizione delle missioni
 */
 
 
 
 // Missione Futura 1

function apriMissioneFutura1() {
	document.getElementById("DescFutur1").classList.remove("hide");
	document.getElementById("DescFutur1").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura1() {
	document.getElementById("DescFutur1").getElementsByTagName("form")[0].classList.add("hide");
	document.getElementById("DescFutur1").classList.add("hide");
}

// Missione Futura 2

function apriMissioneFutura2() {
  document.getElementById("DescFutur2").classList.remove("hide");
  document.getElementById("DescFutur2").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura2() {
  document.getElementById("DescFutur2").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescFutur2").classList.add("hide");
}

// Missione Futura 3

function apriMissioneFutura3() {
  document.getElementById("DescFutur3").classList.remove("hide");
  document.getElementById("DescFutur3").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura3() {
  document.getElementById("DescFutur3").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescFutur3").classList.add("hide");
}

// Missione Futura 4

function apriMissioneFutura4() {
  document.getElementById("DescFutur4").classList.remove("hide");
  document.getElementById("DescFutur4").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura4() {
  document.getElementById("DescFutur4").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescFutur4").classList.add("hide");
}

// Missione Futura 5

function apriMissioneFutura5() {
  document.getElementById("DescFutur5").classList.remove("hide");
  document.getElementById("DescFutur5").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura5() {
  document.getElementById("DescFutur5").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescFutur5").classList.add("hide");
}

// Missione Futura 6

function apriMissioneFutura6() {
  document.getElementById("DescFutur6").classList.remove("hide");
  document.getElementById("DescFutur6").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura6() {
  document.getElementById("DescFutur6").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescFutur6").classList.add("hide");
}

// Missione Futura 7

function apriMissioneFutura7() {
  document.getElementById("DescFutur7").classList.remove("hide");
  document.getElementById("DescFutur7").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura7() {
  document.getElementById("DescFutur7").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescFutur7").classList.add("hide");
}

// Missione Futura 8

function apriMissioneFutura8() {
  document.getElementById("DescFutur8").classList.remove("hide");
  document.getElementById("DescFutur8").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneFutura8() {
  document.getElementById("DescFutur8").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescFutur8").classList.add("hide");
}




// Vecchia Missione 1
function apriVecchiaMissione1() {
  document.getElementById("DescOld1").classList.remove("hide");
  document.getElementById("DescOld1").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione1() {
  document.getElementById("DescOld1").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld1").classList.add("hide");
}

//Vecchia Missione 2

function apriVecchiaMissione2() {
  document.getElementById("DescOld2").classList.remove("hide");
  document.getElementById("DescOld2").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione2() {
  document.getElementById("DescOld2").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld2").classList.add("hide");
}

// Vecchia Missione 3

function apriVecchiaMissione3() {
  document.getElementById("DescOld3").classList.remove("hide");
  document.getElementById("DescOld3").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione3() {
  document.getElementById("DescOld3").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld3").classList.add("hide");
}

// Vecchia Missione 4

function apriVecchiaMissione4() {
  document.getElementById("DescOld4").classList.remove("hide");
  document.getElementById("DescOld4").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione4() {
  document.getElementById("DescOld4").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld4").classList.add("hide");
}

// Vecchia Missione 5

function apriVecchiaMissione5() {
  document.getElementById("DescOld5").classList.remove("hide");
  document.getElementById("DescOld5").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione5() {
  document.getElementById("DescOld5").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld5").classList.add("hide");
}

// Vecchia Missione 6

function apriVecchiaMissione6() {
  document.getElementById("DescOld6").classList.remove("hide");
  document.getElementById("DescOld6").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione6() {
  document.getElementById("DescOld6").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld6").classList.add("hide");
}

// Vecchia Missione 7

function apriVecchiaMissione7() {
  document.getElementById("DescOld7").classList.remove("hide");
  document.getElementById("DescOld7").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione7() {
  document.getElementById("DescOld7").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld7").classList.add("hide");
}

// Vecchia Missione 8

function apriVecchiaMissione8() {
  document.getElementById("DescOld8").classList.remove("hide");
  document.getElementById("DescOld8").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiVecchiaMissione8() {
  document.getElementById("DescOld8").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescOld8").classList.add("hide");
}




// Missione Corrente 1
function apriMissioneCorrente1() {
  document.getElementById("DescCorrent1").classList.remove("hide");
  document.getElementById("DescCorrent1").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente1() {
  document.getElementById("DescCorrent1").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent1").classList.add("hide");
}

// Missione Corrente 2
function apriMissioneCorrente2() {
  document.getElementById("DescCorrent2").classList.remove("hide");
  document.getElementById("DescCorrent2").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente2() {
  document.getElementById("DescCorrent2").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent2").classList.add("hide");
}

// Missione Corrente 3
function apriMissioneCorrente3() {
  document.getElementById("DescCorrent3").classList.remove("hide");
  document.getElementById("DescCorrent3").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente3() {
  document.getElementById("DescCorrent3").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent3").classList.add("hide");
}

// Missione Corrente 4
function apriMissioneCorrente4() {
  document.getElementById("DescCorrent4").classList.remove("hide");
  document.getElementById("DescCorrent4").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente4() {
  document.getElementById("DescCorrent4").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent4").classList.add("hide");
}

// Missione Corrente 5
function apriMissioneCorrente5() {
  document.getElementById("DescCorrent5").classList.remove("hide");
  document.getElementById("DescCorrent5").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente5() {
  document.getElementById("DescCorrent5").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent5").classList.add("hide");
}

// Missione Corrente 6
function apriMissioneCorrente6() {
  document.getElementById("DescCorrent6").classList.remove("hide");
  document.getElementById("DescCorrent6").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente6() {
  document.getElementById("DescCorrent6").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent6").classList.add("hide");
}

// Missione Corrente 7
function apriMissioneCorrente7() {
  document.getElementById("DescCorrent7").classList.remove("hide");
  document.getElementById("DescCorrent7").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente7() {
  document.getElementById("DescCorrent7").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent7").classList.add("hide");
}
// Missione Corrente 8
function apriMissioneCorrente8() {
  document.getElementById("DescCorrent8").classList.remove("hide");
  document.getElementById("DescCorrent8").getElementsByTagName("form")[0].classList.remove("hide");
}

function chiudiMissioneCorrente8() {
  document.getElementById("DescCorrent8").getElementsByTagName("form")[0].classList.add("hide");
  document.getElementById("DescCorrent8").classList.add("hide");
}
