/**
 * @description This script add the 'active' class to some 
 *              html element when 'header-button' is clicked.
 * @author fsociety
 */
(function($){
    $('.header-button').click(function(e){
        e.preventDefault();
        $('.header-button').toggleClass('active');
        $('.header-menu').toggleClass('active');
    })
})(jQuery);