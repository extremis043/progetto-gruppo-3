/**
 * @file: homepage-news-changer.js
 * @author: fsociety
 * Purpose of file
 *
 * This JS file changes the number of news seen by the user based on its devide width.
 */

// Assigning the result of the media query into a constant
const mq = window.matchMedia( "(max-width: 799px)" );

// If the media query matches (if the user's device width is bigger than 799px)
if(mq.matches){
    //Changes the classes of the 2nd and 3rd elements, making visible only the first two
    var a=document.getElementsByClassName("single-news");
    a[1].classList.add("show");
    a[2].classList.remove("show");
    a[2].classList.add("hide");

    /*
        And also changes the number of news visible when scrolling (changing the parameters of
        the onclick attribute)
    */
    document.getElementById("news-forward-btn").setAttribute('onclick','newsSlider(true,1)');
    document.getElementById("news-backward-btn").setAttribute('onclick','newsSlider(false,1)');
}