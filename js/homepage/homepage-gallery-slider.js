/**
 * @file: generic-single-image-slider.js
 * @author: Fsociety
 * Purpose of file
 *
 * This JS file has been created to contain a function that,
 * given a collection of images (shown one at time), makes the
 * collection dynamic.
 */

/**
 * Changes the display property of the figures, based on the button pressed,
 * to go forward or backward.
 * @param {number} imageChanger - The number that makes the function recognize 
 *                                if you want to go forward (1) or backward (-1)
 */
function imageSlider(imageChanger){
    var imageList= document.getElementsByClassName("single-image-section");
    var i,imageSectionLenght=imageList.length;
    
    // searching for the first element that has the class "show" that has a display block property
    for (i=0; i<imageSectionLenght && !imageList[i].classList.contains("show-block"); i++) {
        
    }

    // If the user wants to go forward (1)
    if(imageChanger==1){
        imageList[i].classList.remove("show-block");
        imageList[i].classList.add("hide");
        
        // If the user is viewing the last element and want to go forward
        if(i==imageSectionLenght-1){
            // Show the first element
            imageList[0].classList.add("show-block");
            imageList[0].classList.remove("hide");
        }else{
            imageList[i+1].classList.add("show-block");
            imageList[i+1].classList.remove("hide");
        }
        
    }

    // If the user wants to go backward (1)
    if(imageChanger==-1){
        imageList[i].classList.remove("show-block");
        imageList[i].classList.add("hide");

        // If the user is viewing the first element and want to go backward
        if(i==0){
            // Show the last element
            imageList[imageSectionLenght-1].classList.add("show-block");
            imageList[imageSectionLenght-1].classList.remove("hide");
        }else{
            imageList[i-1].classList.add("show-block");
            imageList[i-1].classList.remove("hide");
        }
    }
}