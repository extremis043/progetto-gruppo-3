/**
 * @file: filename.js
 * @author: FSociety
 * Purpose of file
 *
 * This JS file manage the visibility of the buttons in the navbar
 * when the screen is in the mobile version
 */


 /** 
  *  Returns the sum of num1 and num2 
  *  @param {Number} num1 - the first number 
  *  @param {Number} num2 - the second number 
  *  @returns {Number} Sum of num1 and num2 
  * 
  */
 function showMenu(){
    var currentMenuStatus=document.getElementsByClassName("main-single-element");

    if(currentMenuStatus[0].style.display==""){
        for(var i=0; i< currentMenuStatus.length; i++){
            currentMenuStatus[i].style.display="block";
        }
    }else{
        for(var i=0; i< currentMenuStatus.length; i++){
            currentMenuStatus[i].style.display="";
        } 
    }

 }