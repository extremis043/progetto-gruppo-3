/**
 * @file: generic-multi-element-slider.js
 * @author: Fsociety
 * Purpose of file
 *
 * This JS file contains a function that is used to make a collection
 * of images dynamic.
 */

 /**
  * Changes the class of the images hiding or showing them based on the user pressed button
  * (rappresented by the argument)
  * @param {boolean} imageChanger This argument specifies the action regarding the scroll of the
  *                               images (true if the user go forward, else backward)
  */
 function newsSlider(imageChanger, numElementsShown) {
    
    var newsList= document.getElementsByClassName("single-news");
    var elements= newsList.length, i, position=0;
	
    /*
        The length of the array is assigned to the elements variable in order to avoid multiple 
        accesses to the document class
    */


    // This specifies the number of elements shown at the time (val - 1)
    //var numElementsShown=2;    // MODIFY this variable assigning the number of elements shown at time -1
    

    // Searching the last element that has the class "show" and memorizing it on the position variable
    for(i=0; i<elements ; i++){
        if(newsList[i].classList.contains("show")){
            position=i;
        }
    }




    // If the user wants to go forward
    if(imageChanger==true){
        // The first element visible is hided (adding a CSS class that contains the display property)
        newsList[position-numElementsShown].classList.add("hide");

        /* 
            If the user has reached the end of the news (-1 is used because the length property doesn't count
            that the array starts from 0)
        */
        if(position==elements-1){
            // Hide all elements of the array except for the first shown at the beginning
            for(i=0; i<elements; i++){
                if(i<numElementsShown+1){
                    newsList[i].classList.remove("hide");
					
                }else{
                    newsList[i].classList.add("hide");
                }
                newsList[i].classList.remove("show");
                
            }
            // adding the class to the last element visible so that the first search works again
            newsList[numElementsShown].classList.add("show");
        }else{
            newsList[position+1].classList.add("show");
            newsList[position+1].classList.remove("hide");
        }





    // If the user wants to go backward    
    }else{
        // Check if the user is viewing the first N elements visible
        if(position==numElementsShown){
            // If yes, the last N elements visible are shown
            for(i=elements-1; i>elements-(numElementsShown+2); i--){
                
                newsList[i].classList.remove("hide");
                newsList[i].classList.add("show");
            }

            // And the first N elements visible are hided
            for(i=0; i<numElementsShown+1; i++){
                newsList[i].classList.add("hide");
                newsList[i].classList.remove("show");
            }
        }else{
            // If not, the last element shown is hided
            newsList[position].classList.add("hide");
            newsList[position].classList.remove("show");

            // And the element hided, before the sequence, is shown.
            newsList[position-(numElementsShown+1)].classList.add("show");
            newsList[position-(numElementsShown+1)].classList.remove("hide");
        }
    }
 }